export default {
    "colors": [
        'rgb(255,255,204)', 'rgb(204,255,204)', 'rgb(153,255,204)', 'rgb(204,255,255)', 'rgb(204,229,255)', 'rgb(204,204,229)', 'rgb(229,204,255)', 'rgb(255,204,229)' //'rgb(224,224,224)'
    ],
    "windows": [
         {
             "id": 1,
             "name": "Okno s velkym O",
             "color": 'rgb(224,224,224)',
             "filter":
             {
                 "tags": [],
                 "states": []
             }
         },
        {
            "id": 2,
            "name": "windows s malym W",
            "color": 'rgb(255,229,204)',
            "filter":
            {
                "tags": [ "projekt", "skola" ],
                "states": []
            }
        },
    ],
    "users": [
        {
            "name": "Brano",
            "photoUrl": "https://some.dumb.profile.pic.com"
        },
        {
            "name": "Dominik",
            "photoUrl": "https://some.even.dumber.profile.pic.com"
        },
        {
            "name": "Dan",
            "photoUrl": "https://some.even.dumber.profile.pic.com"
        }
    ],
    "tags": ["skola", "prace", "rodina", "domov", "ach jo", "projekt", "ukol", "volny cas", "urgentni", "do budoucna", "divny"],
    "states": ["novy", "pokracuje", "dokonceny", "potrebuje upravy", "vsehochut"],
    "tasks": [
        {
            "id": "1",
            "name": "Uklid domu",
            "createdAt": "2023-11-24T01:01:00.000Z",
            "deadline": "2023-11-30T01:01:00.000Z",
            "assignedTo": "Dan",
            "description": "Je potreba uklidit nejen na Vanoce.",
            "tags": ["domov", "ach jo"],
            "state": "pokracuje",
            "comments": [
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "He he, nezavidim."
                },
                {
                    "user": {
                        "name": "Brano",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Nezapomen uklidit i za gaucem."
                }
            ]
        },
        {
            "id": "2",
            "name": "Domaci ukoly na PSL",
            "createdAt": "2023-11-17T01:01:00.000Z",
            "deadline": "2023-11-28T01:01:00.000Z",
            "assignedTo": "Brano",
            "description": "Nejake bodiky navic by se hodily.",
            "tags": ["skola", "ukol"],
            "state": "novy",
            "comments": []
        },
        {
            "id": "3",
            "name": "Semestralka na NUR",
            "createdAt": "2023-11-17T01:01:00.000Z",
            "deadline": "2023-11-28T01:01:00.000Z",
            "assignedTo": "Dominik",
            "description": "Zkouska, zkouska - funguje to UI?",
            "tags": ["skola", "projekt"],
            "state": "novy",
            "comments": [
                {
                    "user": {
                        "name": "Dan",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Toto je pokusny komentar.."
                },
                {
                    "user": {
                        "name": "Brano",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "A tohle je komentar s velkym K."
                }
            ]
        },
        {
            "id": "4",
            "name": "Nakup mleka",
            "createdAt": "2023-11-17T01:01:00.000Z",
            "deadline": "2023-11-17T01:10:00.000Z",
            "assignedTo": "Dominik",
            "description": "Takze mleko, slanina, pecivo, banany... ale hlavne to mleko. Bez lupinku s mlekem nevyjde Anet z domu.",
            "tags": ["domov"],
            "state": "dokonceny",
            "comments": [
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Abych nezapomel, ze nebydlim sam :D."
                }
            ]
        },
        {
            "id": "5",
            "name": "Zkontrolovat testovaci data",
            "createdAt": "2023-11-24T01:01:00.000Z",
            "deadline": "2023-11-26T01:10:00.000Z",
            "assignedTo": "Brano",
            "description": "Checknout, jestli mame nejaka smysluplna data, at jenom necteme lorem ipsum - ci ako se to vola.",
            "tags": ["skola", "projekt"],
            "state": "dokonceny",
            "comments": [
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Mas to tam."
                },
                {
                    "user": {
                        "name": "Dan",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Ne ze bysme si musela cist zrovna tvoje nakupni seznamy... rofl"
                },
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Doufam, ze jsou ty zpravy serazeny. Zrovna tuhle ficuru jsme moc neresili. Hlavne tasky."
                }
            ]
        },
        {
            "id": "6",
            "name": "Sesty task",
            "createdAt": "2023-11-24T01:01:00.000Z",
            "deadline": "2023-12-12T01:10:00.000Z",
            "assignedTo": "Dan",
            "description": "Tak trochu mi dochazi napady na tasky... mozna tu zacnu psat pribeh...",
            "tags": ["prace", "projekt"],
            "state": "pokracuje",
            "comments": [
                {
                    "user": {
                        "name": "Brano",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Bez komentare."
                },
                {
                    "user": {
                        "name": "Dan",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Vzdyt tu jeden je"
                },
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Aspon otestujeme delku."
                },
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Hejhola, dva komentare od stejneho autora."
                },
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Ne, dokonce tri."
                },
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Ctyri !!!!"
                },
                {
                    "user": {
                        "name": "Brano",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "To by melo stacit."
                }
            ]
        },
        {
            "id": "7",
            "name": "Task beze jmena",
            "createdAt": "2023-11-24T01:01:00.000Z",
            "deadline": "2023-12-26T01:10:00.000Z",
            "assignedTo": "Brano",
            "description": "Pojdme diskutovat o tasku, co nema jmeno. Takovy task beze jmena preci jen nejake jmeno ma a to, ze je Task beze jmena. To znamena, ze lze filtrovat podle tohoto jmena. Ale co na tom, jak to jmeno zni. Hlavne, ze nema ani tagy, ani komentare. Schovivavy uzivatel necht zkusi za domaci ukol doplnit.",
            "tags": [],
            "state": "dokonceny",
            "comments": [
            ]
        },
        {
            "id": "8",
            "name": "Osm hroznych",
            "createdAt": "2023-11-25T01:01:00.000Z",
            "deadline": "2023-12-24T01:10:00.000Z",
            "assignedTo": "Dan",
            "description": "Je celkem fajn film. Vyrazime na nej. Nejdriv ale sehnat listky.",
            "tags": ["volny cas", "rodina"],
            "state": "novy",
            "comments": [
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Souhlas. Jen nechápu ten deadline 24.12.2023."
                }
            ]
        },
        {
            "id": "9",
            "name": "Dokumentace k projektu",
            "createdAt": "2023-11-01T01:01:00.000Z",
            "deadline": "2023-12-30T01:10:00.000Z",
            "assignedTo": "Dan",
            "description": "Dopsat projektovou dokumentaci.",
            "tags": ["prace", "projekt"],
            "state": "novy",
            "comments": []
        },
        {
            "id": "10",
            "name": "Vytvorit navrh k UI",
            "createdAt": "2023-11-25T01:01:00.000Z",
            "deadline": "2023-12-24T01:10:00.000Z",
            "assignedTo": "Dominik",
            "description": "A ted by se hodilo aplikovat skolni znalosti i v praxi.",
            "tags": ["prace"],
            "state": "potrebuje upravy",
            "comments": []
        },
        {
            "id": "11",
            "name": "11-cty task",
            "createdAt": "2023-10-20T01:01:00.000Z",
            "deadline": "2023-11-24T01:10:00.000Z",
            "assignedTo": "Brano",
            "description": "11 je velmi zajiamve cislo.",
            "tags": ["volny cas", "vsehochut"],
            "state": "urgentni",
            "comments": [
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Velmi urgentni task k nicemu."
                }
            ]
        },
        {
            "id": "12",
            "name": "Nakup auta",
            "createdAt": "2023-11-25T01:01:00.000Z",
            "deadline": "2023-12-24T01:10:00.000Z",
            "assignedTo": "Dominik",
            "description": "Pokud bude mleko, kup i auto... ano, milacku... ale kde na to vezmu :D",
            "tags": ["volny cas", "rodina", "ach jo"],
            "state": "novy",
            "comments": [
                {
                    "user": {
                        "name": "Brano",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Pak me svezes taky."
                }
            ]
        },
        {
            "id": "13",
            "name": "Velmi smutny 13. task",
            "createdAt": "2020-11-25T01:01:00.000Z",
            "deadline": "2023-12-24T01:10:00.000Z",
            "assignedTo": "Dan",
            "description": "V patek 13. Francouzi sli po templarich - jedina valka, co vyhrali.",
            "tags": ["volny cas", "rodina", "do budoucna"],
            "state": "dokonceny",
            "comments": [
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "True story."
                }
            ]
        },
        {
            "id": "14",
            "name": "Kill Bill",
            "createdAt": "2023-11-25T01:01:00.000Z",
            "deadline": "2023-12-24T01:10:00.000Z",
            "assignedTo": "Dan",
            "description": "Dalsi film od Tarantina - old but gold - pujdeme?",
            "tags": ["volny cas"],
            "state": "novy",
            "comments": [
                {
                    "user": {
                        "name": "Brano",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Well yes, but actually no."
                }
            ]
        },
        {
            "id": "15",
            "name": "5x3 task",
            "createdAt": "2023-10-01T01:01:00.000Z",
            "deadline": "2024-10-24T01:10:00.000Z",
            "assignedTo": "Brano",
            "description": "Na tento task je plno casu, presto ma tag urgentni.",
            "tags": ["urgentni"],
            "state": "pokracuje",
            "comments": [
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Snad ten task stihneme."
                }
            ]
        },
        {
            "id": "16",
            "name": "Task",
            "createdAt": "2023-11-25T01:01:00.000Z",
            "deadline": "2023-12-24T01:10:00.000Z",
            "assignedTo": "Brano",
            "description": "Co vic rict.",
            "tags": ["volny cas", "rodina", "prace"],
            "state": "novy",
            "comments": [
            ]
        },
        {
            "id": "17",
            "name": "Task bez deadline neni mozny",
            "createdAt": "2023-11-25T01:01:00.000Z",
            "deadline": "2023-11-25T01:01:00.000Z",
            "assignedTo": "Dan",
            "description": "Proste ten deadline chybi... Ale bylo treba ho doplnit",
            "tags": ["divny", "vsehochut"],
            "state": "novy",
            "comments": [
            ]
        },
        {
            "id": "18",
            "name": "Task se stejnymi datumy",
            "createdAt": "2023-12-24T01:01:00.000Z",
            "deadline": "2023-12-24T01:10:00.000Z",
            "assignedTo": "Brano",
            "description": "Zvlastni, ten task mel byt hotov v okamziku zadani. Kdo to nezna z prace...",
            "tags": ["divny"],
            "state": "dokonceny",
            "comments": [
            ]
        },
        {
            "id": "19",
            "name": "Task bez tagu",
            "createdAt": "2022-11-25T01:01:00.000Z",
            "deadline": "2023-12-24T01:10:00.000Z",
            "assignedTo": "Dan",
            "description": "Vubec zadny TAGY !!!",
            "tags": [],
            "state": "novy",
            "comments": [
            ]
        },
        {
            "id": "20",
            "name": "Task bez popisu",
            "createdAt": "2023-10-25T01:01:00.000Z",
            "deadline": "2023-12-24T01:10:00.000Z",
            "assignedTo": "Dominik",
            "description": "",
            "tags": ["divny"],
            "state": "pokracuje",
            "comments": [
            ]
        },
        {
            "id": "21",
            "name": "Task s ukrutne dlouhym nazvem a nekolika teckami",
            "createdAt": "2023-11-25T01:01:00.000Z",
            "deadline": "2023-12-24T01:10:00.000Z",
            "assignedTo": "Brano",
            "description": "Tak dlouhy popis si ten task snad nezaslouzil.",
            "tags": ["prace", "do budoucna", "divny"],
            "state": "novy",
            "comments": [
            ]
        },
        {
            "id": "22",
            "name": "Task se vsemi tagy",
            "createdAt": "2023-11-25T01:01:00.000Z",
            "deadline": "2023-12-24T01:10:00.000Z",
            "assignedTo": "Dan",
            "description": "NAPROSTO VSECHNY TAGY !!!",
            "tags": ["skola", "prace", "rodina", "domov", "ach jo", "projekt", "ukol", "volny cas", "urgentni", "do budoucna", "divny"],
            "state": "novy",
            "comments": [
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Stejny komentar"
                },
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Stejny komentar"
                },
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Stejny komentar"
                },
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Stejny komentar"
                },
                {
                    "user": {
                        "name": "Dominik",
                        "photoUrl": "https://some.dumb.profile.pic.com"
                    },
                    "text": "Stejny komentar"
                }
            ]
        },
        {
            "id": "23",
            "name": "Jeden za 23 a druhý bez jedne za 24",
            "createdAt": "2022-11-25T01:01:00.000Z",
            "deadline": "2024-12-24T01:10:00.000Z",
            "assignedTo": "Dan",
            "description": "Sorry jako.",
            "tags": ["rodina", "vsehochut"],
            "state": "urgentni",
            "comments": [
            ]
        },
        {
            "id": "24",
            "name": "24 hodin",
            "createdAt": "2023-08-25T01:01:00.000Z",
            "deadline": "2023-09-24T01:10:00.000Z",
            "assignedTo": "Dan",
            "description": "Tolik hodin jako na kostele.",
            "tags": ["volny cas", "rodina", "prace", "vsehochut"],
            "state": "pokracuje",
            "comments": [
            ]
        }
    ]
}