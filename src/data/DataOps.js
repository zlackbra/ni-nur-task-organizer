import dayjs from "dayjs";

export function sortTasks(tasks, sort) {
    if (sort == null) {
        return tasks
    }

    return tasks.sort((a, b) => {
        if (sort.name === "asc") {
            return a.name.localeCompare(b.name);
        } else if (sort.name === "desc") {
            const result = a.name.localeCompare(b.name);
            if (result === 0) {
                return 0;
            } else if (result === -1) {
                return 1;
            } else {
                return -1;
            }
        } else {
            return 0
        }
    }).sort((a, b) => {
        if (sort.createdAt === "asc") {
            const isBefore = dayjs(a.createdAt).isBefore(b.createdAt);

            if (isBefore) {
                return -1;
            } else {
                return 1;
            }
        } else if (sort.createdAt === "desc") {
            const isBefore = dayjs(a.createdAt).isBefore(b.createdAt);

            if (isBefore) {
                return 1;
            } else {
                return -1;
            }
        } else {
            return 0
        }
    }).sort((a, b) => {
            if (sort.deadline === "asc") {
                if (a.deadline === null || b.deadline === null) {
                    return 0;
                }

                const isBefore = dayjs(a.deadline).isBefore(b.deadline);

                if (isBefore) {
                    return -1;
                } else {
                    return 1;
                }
            } else if (sort.deadline === "desc") {
                if (a.deadline === null || b.deadline === null) {
                    return 0;
                }

                const isBefore = dayjs(a.deadline).isBefore(b.deadline);

                if (isBefore) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                return 0
            }
        }
    )
}


export function filterTasks(tasks, filter) {
    if (filter == null || filter.length === 0) {
        return tasks
    }

    const checkDate = (option, value) => {
        if (option === null) {
            return true;
        }
        if (value === null) {
            return false
        }

        const from = option.from ? option.from : null
        const to = option.to ? option.to : null

        const meetsFrom = from === null ? true : dayjs(from).isBefore(dayjs(value));
        const meetsTo = to === null ? true : dayjs(to).isAfter(dayjs(value));

        return meetsFrom && meetsTo;
    };

    const tags = filter.tags ? filter.tags : []
    const name = filter.name ? filter.name : []
    const states = filter.states ? filter.states : []
    const createdAt = filter.createdAt ? filter.createdAt : null
    const deadline = filter.deadline ? filter.deadline : null
    const assignedTo = filter.assignedTo ? filter.assignedTo : []

    return tasks.filter(task => {
        const meetsTags = !tags.length ? true : task.tags.find(el => filter.tags.includes(el)) !== undefined;
        const meetsName = !name.length ? true : filter.name.includes(task.name);
        const meetsStates = !states.length ? true : filter.states.includes(task.state);
        const meetsCreatedAt = checkDate(createdAt, task.createdAt);
        const meetsDeadlineAt = checkDate(deadline, task.deadline);
        const meetsAssignedTo = !assignedTo.length ? true : filter.assignedTo.includes(task.assignedTo);

        return meetsTags && meetsName && meetsStates && meetsCreatedAt && meetsDeadlineAt && meetsAssignedTo;
    });
}


export function removeDeletedStatesEverywhere(currWindows, currTasks, updatedStates) {
    const windows = currWindows.map(window => {
        const filterStates = window.filter.states.filter(other => updatedStates.includes(other));
        return {...window, filter: {...window.filter, states: filterStates}};
    });

    const tasks = currTasks.map(task => {
        if (updatedStates.includes(task.state)) {
            return task;
        } else {
            return {...task, state: null};
        }
    });

    return {windows: windows, tasks: tasks, states: updatedStates}
}


export function removeDeletedTagsEverywhere(currWindows, currTasks, updatedTags) {
    const windows = currWindows.map(window => {
        const filterTags = window.filter.tags.filter(other => updatedTags.includes(other));
        return {...window, filter: {...window.filter, tags: filterTags}};
    });

    const tasks = currTasks.map(task => {
        const taskTags = task.tags ? task.tags.filter(other => updatedTags.includes(other)) : []
        return {...task, tags: taskTags};
    });

    return {windows: windows, tasks: tasks, tags: updatedTags}
}
