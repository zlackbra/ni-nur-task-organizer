export default {
    "colors": [
        'rgb(255,255,204)', 'rgb(204,255,204)', 'rgb(153,255,204)', 'rgb(204,255,255)', 'rgb(204,229,255)', 'rgb(204,204,229)', 'rgb(229,204,255)', 'rgb(255,204,229)' //'rgb(224,224,224)'
    ],
    "windows": [
    ],
    "users": [
        {
            "name": "Brano",
            "photoUrl": "https://some.dumb.profile.pic.com"
        },
        {
            "name": "Dominik",
            "photoUrl": "https://some.even.dumber.profile.pic.com"
        },
        {
            "name": "Dan",
            "photoUrl": "https://some.even.dumber.profile.pic.com"
        }
    ],
    "tags": [],
    "states": [],
    "tasks": [] 
}