export const MinHorizontalWidth = 850;
export const TabRowWidth = 500;

export const MaxWindows = 10;

export const TagBgColor = 'rgba(200, 255, 200)';
export const StateBgColor = 'rgba(255, 255, 200)';

export const TaskCardFontSize = '90%';

export const TagCustomStyles = {
    control: (base) => ({
        ...base,
        background: TagBgColor,
        width: '230px',
    }),
    valueContainer: (base) => ({
        ...base,
        maxHeight: '65px',
        overflowY: 'auto',
    }),
    multiValue: (base) => ({
        ...base,
        marginBottom: '5px',
        marginTop: '5px'
    })
};
