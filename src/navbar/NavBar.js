import {useState} from "react";
import "./NavBar.css"
import HelpIcon from '@mui/icons-material/Help';
import SettingsIcon from '@mui/icons-material/Settings';
import NotificationsIcon from '@mui/icons-material/Notifications';
import GroupsIcon from '@mui/icons-material/Groups';

const NavBar = () => {
    const [menuActive, setMenuActive] = useState("nav-menu");
    const [toggleMenuIcon, setToggleMenuIcon] = useState("nav-toggler")

    const navToggle = () => {
        menuActive === "nav-menu" ? setMenuActive("nav-menu nav-active") : setMenuActive("nav-menu")
        toggleMenuIcon === "nav-toggler" ? setToggleMenuIcon("nav-toggler toggle") : setToggleMenuIcon("nav-toggler")
    }

    return (
        <div className="rectangle">
            <nav className="nav">
                <h1 className="nav-bar-name">Task Organizer</h1>
                <div className="nav-bar-right">
                    <ul className={menuActive}>
                        <div className="nav-bar-icons">
                            <div className="nav-bar-groups">
                                <GroupsIcon fontSize='large'/>
                                <h5>Members</h5>
                            </div>
                            <div className="nav-bar-notifications">
                                <NotificationsIcon fontSize='large'/>
                                <h5>Notifications</h5>
                            </div>
                            <div className="nav-bar-settings">
                                <SettingsIcon fontSize='large'/>
                                <h5>Settings</h5>
                            </div>
                            <div className="nav-bar-help">
                                <HelpIcon fontSize='large'/>
                                <h5>Help</h5>
                            </div>
                        </div>
                    </ul>
                    <div onClick={navToggle} className={toggleMenuIcon}>
                        <div className="line1"></div>
                        <div className="line2"></div>
                        <div className="line3"></div>
                    </div>
                    <img className="nav-bar-profile-pic"
                         src="https://i.pinimg.com/originals/6b/e8/6e/6be86ee43bb016e3e04bdadbb121871f.jpg"
                         alt="Cat profile pic"/>
                </div>
            </nav>
        </div>
    );
}

export default NavBar;