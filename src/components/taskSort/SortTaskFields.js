import {useContext, useEffect, useState} from "react";
import {SortTaskContext, SortTaskDispatchContext} from "./SortTaskContext";
import {Grid, ToggleButton, ToggleButtonGroup, Typography} from "@mui/material";



function SortTaskField({ initValue, setValue }) {
    const [value, setThisValue] = useState(initValue);

    const handleChange = (event, value) => {
        setThisValue(value);
        setValue(value);
    };

    return (
        <ToggleButtonGroup
            size="small"
            color="primary"
            value={value}
            exclusive
            onChange={handleChange}
            aria-label="Platform"
        >
            <ToggleButton value="desc">descending</ToggleButton>
            <ToggleButton value="nothing">---</ToggleButton>
            <ToggleButton value="asc">ascending</ToggleButton>
        </ToggleButtonGroup>
    );
}


export function SortTaskFields() {
    const sort = useContext(SortTaskContext);
    const dispatch = useContext(SortTaskDispatchContext);

    return (
        <>
            <Grid container rowSpacing={2}  alignItems="center">
                <Grid item xs={6}>
                    <Typography variant="body1">
                        By name
                    </Typography>
                </Grid>
                <Grid item xs={6} >
                    <SortTaskField
                        initValue={sort.name}
                        setValue={ (value) => dispatch({ type: "setName", item: value }) }
                    />
                </Grid>

                <Grid item xs={6}  >
                    <Typography variant="body1">
                        By created date
                    </Typography>
                </Grid>
                <Grid item xs={6}>
                    <SortTaskField
                        initValue={sort.createdAt}
                        setValue={ (value) => dispatch({ type: "setCreatedAt", item: value }) }
                    />
                </Grid>

                <Grid item xs={6} >
                    <Typography variant="body1">
                        By deadline date
                    </Typography>
                </Grid>
                <Grid item xs={6}>
                    <SortTaskField
                        initValue={sort.deadline}
                        setValue={ (value) => dispatch({ type: "setDeadlineAt", item: value }) }
                    />
                </Grid>
            </Grid>
        </>
    );
}