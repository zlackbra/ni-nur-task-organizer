export function SortReducer(sort, action) {
    switch (action.type) {
        case "setName": {
            return {...sort, name: action.item};
        }

        case "setCreatedAt": {
            return {...sort, createdAt: action.item};
        }

        case "setDeadlineAt": {
            return {...sort, deadline: action.item};
        }

        default: {
            throw Error('Unknown action: ' + action.type);
        }
    }
}