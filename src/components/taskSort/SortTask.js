import Button from "@mui/material/Button";
import {useReducer} from "react";
import {SortTaskContext, SortTaskDispatchContext} from "./SortTaskContext";
import {SortReducer} from "./SortTaskReducer";
import {SortTaskFields} from "./SortTaskFields";
import "./style/SortTask.css"
import Dialog from "@mui/material/Dialog";
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Cancel';

function Header({ window }) {
    return (
        <div className="header" style={{marginBottom: '30px'}}>
            <h1>{"Task Sort for " + window}</h1>
        </div>
    );
}


function Footer({ save, close }) {
    return (
        <div className="footer">
            <div className="buttons">
                <Button variant="contained" startIcon={<SaveIcon/>} onClick={save}>
                    Save
                </Button>
                <Button variant="outlined" startIcon={<CancelIcon/>} onClick={close}>
                    Close
                </Button>
            </div>
        </div>
    );
}

const createDefaultSort = () => {
    return {
        name: "nothing",
        createdAt: "nothing",
        deadline: "nothing"
    }
}


export function SortTask({ window, updateWindow, close }) {
    const [sort, dispatch] = useReducer(SortReducer, window.sort ? window.sort : createDefaultSort());

    const updateSortInWindow = () => {
        window.sort = sort

        updateWindow(window)
    }

    const saveAndClose = () => {
        updateSortInWindow(sort)
        close()
    }

    const handleKeyDown = (event) => {
        if (event.key === 'Enter'){
            saveAndClose();
        }
        else if (event.key === 'Escape'){
            close();
        }
      };


    return (
        <Dialog
            open={true}
            fullWidth
            maxWidth="sm"
            onKeyDown={handleKeyDown}
        >
            <div className="sort-dialog">
                <Header window={window.name} />

                <SortTaskContext.Provider value={sort}>
                    <SortTaskDispatchContext.Provider value={dispatch}>
                        <SortTaskFields />
                    </SortTaskDispatchContext.Provider>
                </SortTaskContext.Provider>

                <Footer close={close} save={saveAndClose} />
            </div>
        </Dialog>
    );
}
