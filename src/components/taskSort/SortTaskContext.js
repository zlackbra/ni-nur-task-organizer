import { createContext } from 'react';

export const SortTaskContext = createContext(null);
export const SortTaskDispatchContext = createContext(null);
