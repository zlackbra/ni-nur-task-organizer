import './style/Header.css'
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';

const Header = ({name, close}) => {
    return (
        <div className="header">
            <h1>{name}</h1>
            <div className="button">
                <IconButton variant="outlined" onClick={close}>
                <CloseIcon/>
                </IconButton>
            </div>
        </div>
    )
}

export default Header;