import {v4 as uuid} from "uuid";

const createTab = (color) => {
    const id = uuid()

    return {
        name: 'untitled', 
        id: id, 
        color: color,
        filter: {tags: [], states: []}
    };
}

export default createTab;