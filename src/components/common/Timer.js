import React from 'react';
import { useTimer } from 'react-timer-hook';

function MyTimer({expiryTimestamp, onExpire}) {
    const {
      totalSeconds,
      seconds,
      minutes,
      hours,
      days,
      isRunning,
      start,
      pause,
      resume,
      restart,
    } = useTimer({expiryTimestamp, onExpire: () => onExpire()});
  
  
    return (
      <div>
          <span>&#40;{seconds}s&#41;</span>
      </div>
    );
  }

export default MyTimer;