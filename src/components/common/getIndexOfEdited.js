const getIndexOfEdited = (items, id) => {
    console.log(id);
    for (let i = 0; i < items.length; i++) {
        if (items[i].id === id) {
            return i
        }
    }

    throw new DOMException("Edited task not found!")
}

export default getIndexOfEdited
