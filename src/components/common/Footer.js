import './style/Footer.css'
import Button from '@mui/material/Button';
import CancelIcon from '@mui/icons-material/Cancel';

const Footer = ({close, buttons}) => {
    return (
        <div className="footer">
            <div className="buttons">
                {buttons}
                <Button variant="outlined"  onClick={close} startIcon={<CancelIcon/>}  style={{ minWidth: '100px'}}>
                    Close
                </Button>
            </div>
        </div>
    )
}

export default Footer;