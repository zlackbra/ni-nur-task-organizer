import './style/Assign.css'

const Assign = ({name}) => {
    return (<>
        <h2>Assign To</h2>
        <div className='assign-label'>{name}</div>

        </>
    )
}

export default Assign;