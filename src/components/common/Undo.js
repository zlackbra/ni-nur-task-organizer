import { Button } from '@mui/material';
import './style/Undo.css'
import MyTimer from './Timer';

const Undo = (props) => {

    const setTimestamp = () => {
        const time = new Date();
        time.setSeconds(time.getSeconds() + 10);
        return time;
    }

    return (
        <div className='undo'>
            <div className='undo-right'>
                <div className='undo-text'>
                    Task "{props.name}" has been deleted
                </div>
                <Button variant='outlined' onClick={props.undoDelete}
                    style={{maxHeight:'80%'}}
                >Undo&nbsp;<MyTimer expiryTimestamp={setTimestamp} onExpire={props.clearUndo}/></Button>
            </div>
        </div>
    )
}

export default Undo;