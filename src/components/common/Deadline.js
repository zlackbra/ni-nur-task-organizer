import {DatePicker, MobileDateTimePicker} from "@mui/x-date-pickers";
import './style/Deadline.css'
import dayjs from "dayjs";

const Deadline = ({date, readOnly=0}) => {
    return (
        <div className="deadline">
            <h2>Deadline</h2>
            <MobileDateTimePicker 
                value={dayjs(date)}
                format='YYYY-MM-DD HH:mm'
                disabled={readOnly}/>
        </div>
    )
}

export default Deadline;