import './style/Footer.css'
import Button from '@mui/material/Button';
import {red} from "@mui/material/colors";
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Cancel';
import DeleteIcon from '@mui/icons-material/Delete';

const Footer = (props) => {
    return (
        <div className="footer-small">
            <div className="footer-buttons">
                <div className="buttons-left">
                    <Button variant="contained" onClick={props.openDelete} startIcon={<DeleteIcon/>} style={{ minWidth: '100px', background: 'red'}}>
                        Delete
                    </Button>
                </div>
                <div className="buttons-right">
                    <h5 className={props.valid}>
                        Name field must be filled and less than 50 characters long!
                    </h5>
                    <Button variant="contained" onClick={props.save} startIcon={<SaveIcon/>} style={{ minWidth: '100px'}}>
                        Save
                    </Button>
                    <Button variant="outlined" onClick={props.close} startIcon={<CancelIcon/>}  style={{ minWidth: '100px'}}>
                        Close
                    </Button>
                </div>
            </div>
        </div>
    )
}

export default Footer;