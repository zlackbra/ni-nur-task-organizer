import './style/Fields.css'
import Description from "./fieldrows/Description";
import Name from "./fieldrows/Name";
import Assign from "./fieldrows/Assign";
import CreatedAt from "./fieldrows/CreatedAt";
import Deadline from "./fieldrows/Deadline";
import State from "./fieldrows/State";
import Tags from "./fieldrows/Tags";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import {LocalizationProvider} from "@mui/x-date-pickers";
import {Grid} from "@mui/material";
import CommentList from "./fieldrows/CommentList";
import CommentWrite from "./fieldrows/CommentWrite";
import {useEffect, useState} from "react";

const Fields = (props) => {
    const [fieldsTable, setFieldsTable] = useState(
        window.innerWidth > 850 ? "fields-table" : "fields-column"
    )

    useEffect(() => {
        function handleResize() {
            setFieldsTable(window.innerWidth > 1100 ? "fields-table" : "fields-column")
        }

        window.addEventListener("resize", handleResize)

        return _ => {
            window.removeEventListener('resize', handleResize)
        }
    })

    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
            <Grid container alignItems="flex-start" marginTop='1vh' rowSpacing={1}
                  columnSpacing={{xs: 1, sm: 2, md: 3}}>
                <Grid item width='50%' maxHeight='60vh' overflow='auto'>
                    <div className={fieldsTable}>
                        <div className="name-assign">
                            <Name setName={props.setName} name={props.name}/>
                            <Assign setAssign={props.setAssign} assign={props.assign}/>
                        </div>
                        <div className="dates-labels">
                            <div className="dates">
                                <div>
                                    <h2>Dates</h2>
                                </div>
                                <CreatedAt createdAt={props.createdAt}/>
                                <Deadline setDeadline={props.setDeadline} deadline={props.deadline}/>
                            </div>
                            <div className="tags-state">
                                <Tags setTags={props.setTags} tags={props.tags} addTag={props.addTag}/>
                                <State setState={props.setState} state={props.state} addState={props.addState}/>
                            </div>
                        </div>
                    </div>
                    <Description setDescription={props.setDescription} description={props.description} blur={props.blur}
                                      focus={props.focus}/>
                </Grid>
                <Grid sx={{borderLeft: 'solid gray'}} item width='50%' maxHeight='100vh' overflow='auto'>
                    <Grid item maxHeight='60%'>
                        <CommentList comments={props.comments}/>
                    </Grid>
                    <Grid item maxHeight='40%'>
                        <CommentWrite comment={{name: "You", text: ""}} send={props.addComment} blur={props.blur}
                                      focus={props.focus}/>
                    </Grid>
                </Grid>
            </Grid>
        </LocalizationProvider>
    )
}

export default Fields;