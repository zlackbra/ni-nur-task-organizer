import './style/CommentText.css'

const CommentText = ({text}) => {
    return (
        <div className="commenttext ">
            <textarea readOnly={true} disabled={true} className="text" value={text}></textarea>
        </div>
    )
}

export default CommentText;