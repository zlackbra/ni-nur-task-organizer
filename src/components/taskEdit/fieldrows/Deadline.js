import {DateTimePicker} from "@mui/x-date-pickers";
import './style/Deadline.css'
import dayjs from "dayjs";

const Deadline = ({setDeadline, deadline}) => {
    const dateWithoutT = deadline ? dayjs(deadline) : ""

    const validateDate = (newValue) => {
        if (newValue == null || newValue.toString() != 'Invalid Date') {
            setDeadline(newValue)
        }
    }

    return (
        <div className="deadline">
            <h3>Deadline</h3>
            <DateTimePicker
                onChange={validateDate}
                ampm={false}
                value={dateWithoutT}
                format='YYYY-MM-DD HH:mm'
                //slotProps={{textField: {placeholder: dateWithoutT}}}
                slotProps={{
                    actionBar: {
                      actions: ['today', 'accept', 'cancel']
                    }
                  }}
            />
        </div>
    )
}

export default Deadline;