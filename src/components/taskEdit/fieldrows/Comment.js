import './style/Comment.css'
import CommentHeader from "./CommentHeader";
import CommentText from "./CommentText";


const Comment = ({comment}) => {
    return (
        <div className="comment">
            <CommentHeader name={comment.user.name}/>
            <CommentText text={comment.text}/>
        </div>
    )
}

export default Comment;