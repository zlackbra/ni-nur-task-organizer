import './style/Description.css'

const Description = ({setDescription, description, blur, focus}) => {
    return (
        <div className="description-edit">
            <h2>Description</h2>
            <textarea className="text" value={description} onChange={event => setDescription(event.target.value)}
            onFocus={focus} onBlur={blur}/>
        </div>
    )
}

export default Description;