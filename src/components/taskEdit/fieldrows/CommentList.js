import './style/CommentList.css'
import Comment from './Comment';
import { Grid } from '@mui/material';


const CommentList = ({comments}) => {
    return (
        <div className="commentlist">
            <h2>Comments</h2>
            <Grid container  direction='column'>
                {comments && comments.map((comment, index) => {
                    return (
                        <Comment key={index} comment={comment}/>
                    );
                })}
            </Grid>
        </div>
    )
}

export default CommentList;