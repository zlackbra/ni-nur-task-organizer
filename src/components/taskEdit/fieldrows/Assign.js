import './style/Assign.css'
import React from "react";
import Select from "react-select";

const customStyles = {
    control: (base) => ({
        ...base,
        width: '230px'
    })
};

const Assign = ({setAssign, assign}) => {
    const usersOptions = JSON.parse(localStorage.getItem("data")).users.map(user =>
        ({value: user.name, label: user.name})
    )

    const alreadyAssigned = ({value: assign, label: assign})

    const onAssignChange = (newValue) => newValue ? setAssign(newValue.value) : setAssign(null)

    return (
        <div className="assign">
            <h2>Assign To</h2>
            <Select
                styles={customStyles}
                value={alreadyAssigned}
                options={usersOptions}
                onChange={(newValue) => onAssignChange(newValue)}
            />
        </div>
    )
}

export default Assign;