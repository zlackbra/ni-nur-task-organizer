import './style/State.css'
import CreatableSelect from 'react-select/creatable';
import React, {useState} from "react";
import {StateBgColor} from '../../../consts/WindowConsts'

const customStyles = {
    control: (base) => ({
        ...base,
        background: StateBgColor,
        width: '230px'
    })
};

const createOption = (label) => ({
    label,
    value: label.toLowerCase().replace(/\W/g, ''),
});

const State = ({setState, state, addState}) => {
    const states = JSON.parse(localStorage.getItem("data")).states
    const initialStatesOptions = states ? states.map(state =>
        ({value: state, label: state})
    ) : []

    const alreadyAddedState = state ? ({value: state, label: state}) : null

    const [options, setOptions] = useState(initialStatesOptions);
    const [value, setValue] = useState(alreadyAddedState);

    const handleCreate = (inputValue) => {
        if (inputValue.trim().length === 0) {
            return
        }

        const newOption = createOption(inputValue);
        setOptions((prev) => [...prev, newOption]);

        addState(inputValue)

        onStateChange(newOption)
    };

    const onStateChange = (newValue) => {
        setValue(newValue)

        newValue ? setState(newValue.value) : setState(null)
    }

    return (
        <div className="state">
            <h2>State</h2>
            <CreatableSelect
                isClearable
                options={options}
                onChange={(newValue) => onStateChange(newValue)}
                styles={customStyles}
                onCreateOption={handleCreate}
                value={value}
            />
        </div>
    )
}

export default State;