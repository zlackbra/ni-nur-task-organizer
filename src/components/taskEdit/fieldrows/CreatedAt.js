import {DateTimePicker, MobileDateTimePicker} from "@mui/x-date-pickers";
import './style/CreatedAt.css'
import dayjs from "dayjs";

const CreatedAt = ({createdAt}) => {
    const dateWithoutT = createdAt ? dayjs(createdAt) : ""

    return (
        <div className="created-at">
            <h3>Created At</h3>
            <MobileDateTimePicker
                disabled={true}
                defaultValue={dayjs(createdAt)}
                format='YYYY-MM-DD HH:mm'
                //slotProps={{textField: {placeholder: dateWithoutT}}}
            />
        </div>
    )
}

export default CreatedAt;