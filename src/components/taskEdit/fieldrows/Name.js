import './style/Name.css'
import TextField from "@mui/material/TextField";

const Name = ({setName, name, blur, focus}) => {
    return (
        <div className="name">
            <div className="name-title">
                <h2>Name</h2>
                <h2 className="required">*</h2>
            </div>
            <TextField
                style={{background: "white", borderRadius: '5px', width: "225px"}}
                size="small"
                error={ name == null || name.trim().length === 0 }
                value={name}
                variant="outlined"
                onChange={ (event) => setName(event.target.value) }
            ></TextField>
        </div>
    )
}

export default Name