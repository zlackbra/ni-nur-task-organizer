import './style/CommentHeader.css'

const CommentHeader = ({name}) => {
    return (
        <div className="commentheader">
            <textarea disabled={true} value={name}/>
        </div>
    )
}

export default CommentHeader;