import './style/CommentWrite.css'
import Button from '@mui/material/Button';
import { useState } from 'react';
import SendIcon from '@mui/icons-material/Send';
import CommentHeader from "./CommentHeader";


const CommentWrite = ({comment, send, blur, focus}) => {
    const [message, setMessage] = useState("")
    const messageMaxLength = 140;

    const sendComment = () => {
        comment = {
            text: message,
            user: {name: 'You', photoUrl: ""}
        }
        send(comment)
        setMessage("")
    }



    return (
        <div className="commentwrite">
            <div className='line'/>
            <CommentHeader name={comment.name}/>
            <textarea className='text' placeholder='Write a comment' value={message} onChange={(e)=>{setMessage(e.target.value)}}
                onBlur={blur} onFocus={focus}
            ></textarea>
            <div className='sendbutton'>
                <Button onClick={sendComment} disabled={message === "" || message.length > messageMaxLength} variant='contained' startIcon={<SendIcon/>}>Send</Button>
            </div>
            <div style={{
                       display: (message.length > messageMaxLength) ? 'flex' : 'none',  
                       textAlign: 'center',
                       justifyContent: 'center',
                       width: '100%',
                       marginTop: '2%',
                       color: 'red'
                    }}
                    >
                        Message too long &#40;{message.length}/{messageMaxLength}&#41;
                </div>
            
        </div>
    )
}

export default CommentWrite;