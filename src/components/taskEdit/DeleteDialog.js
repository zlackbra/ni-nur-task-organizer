import { useState } from "react";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { Button, DialogContentText } from "@mui/material";
import CancelIcon from '@mui/icons-material/Cancel';
import DeleteIcon from '@mui/icons-material/Delete';

const DeleteDialog = (props) => {
    // {open, task, delete, close}

    const dialogDeleteTask = () => {
        props.close()
        props.delete()
    }

    const handleKeyDown = (event) => {
        if (event.key === 'Enter'){
            dialogDeleteTask();
        }
        else if (event.key === 'Escape'){
            props.close();
        }
      };


    return (
        <Dialog open={props.open} onClose={props.close} onKeyDown={handleKeyDown}>
        <DialogTitle>Delete Task</DialogTitle>
            <DialogContent>
            <DialogContentText>
                Are you sure you want to delete Task: <b>{props.task.name}</b>?
            </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button variant='contained' onClick={dialogDeleteTask} startIcon={<DeleteIcon/>}>Delete</Button>
                <Button onClick={props.close} variant="outlined" startIcon={<CancelIcon/>}>Close</Button>
        </DialogActions>
    </Dialog>
);
}


export default DeleteDialog;