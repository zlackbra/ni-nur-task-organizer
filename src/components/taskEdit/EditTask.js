import "./style/EditTask.css"
import {useState} from "react";
import Footer from "./Footer";
import Header from "./Header";
import Fields from "./Fields";
import Dialog from "@mui/material/Dialog";
import DeleteDialog from "./DeleteDialog";

const EditTask = ({task, updateTask, removeTask, close, updateComments, addTag, addState}) => {
    const [isInvalid, setIsInvalid] = useState(false)
    const [name, setName] = useState(task.name)
    const createdAt = task.createdAt
    const [deadline, setDeadline] = useState(task.deadline)
    const [assign, setAssign] = useState(task.assignedTo)
    const [description, setDescription] = useState(task.description ? task.description : "")
    const [tags, setTags] = useState(task.tags)
    const [state, setState] = useState(task.state)
    const [deleteDialog, setDeleteDialog] = useState(false)
    const comments = task.comments ? task.comments : []

    const [inputFocused, setInputFocused] = useState(false)
    const blur = () => setInputFocused(false)
    const focus = () => setInputFocused(true)

    const save = () => {
        // validation
        if (name == null || name === "" || name.length > 50 || name.trim().length === 0) {
            setIsInvalid(true)
        } else {
            close()

            const newTask = {
                id: task.id,
                name: name.trim(), // remove whitespace from both ends of name
                createdAt: createdAt,
                deadline: typeof deadline == 'string' ? deadline : deadline.toISOString(),
                assignedTo: assign,
                description: description,
                tags: tags,
                state: state,
                comments: task.comments
            }

            updateTask(newTask)
        }
    }

    const handleKeyDown = (event) => {
        if (inputFocused || deleteDialog) { return }
        if (event.key === 'Enter'){
            save();
        }
        else if (event.key === 'Escape'){
            close();
        }
        else if (event.key === 'Delete'){
            openDelete();
        }
    };

    const addComment = (newComment) => {
        task.comments = task.comments ? task.comments : []
        // updateComments(newComment)
        task.comments.push(newComment);
        updateTask(task);
    }

    const deleteTask = () => {
        removeTask(task)
    }

    const openDelete = () => {
        setDeleteDialog(true)
    }

    const closeDelete = () => {
        setDeleteDialog(false)
    }

    return (
        <Dialog
            open={true}
            fullWidth
            maxWidth="xl"
            onKeyDown={handleKeyDown}
        >
            <div className="edit-task-dialog">
                <Header/>
                <Fields
                    setName={setName}
                    setDeadline={setDeadline}
                    setAssign={setAssign}
                    setDescription={setDescription}
                    setTags={setTags}
                    setState={setState}
                    addComment={addComment}
                    createdAt={createdAt}
                    name={name}
                    deadline={deadline}
                    assign={assign}
                    description={description}
                    tags={tags}
                    state={state}
                    comments={comments}
                    blur={blur}
                    focus={focus}
                    addTag={addTag}
                    addState={addState}
                />
                <DeleteDialog
                    open={deleteDialog}
                    close={closeDelete}
                    delete={deleteTask}
                    task={task}
                />
                <Footer 
                    close={close}
                    save={save} 
                    valid={isInvalid ? "edit-task-invalid" : "edit-task-valid"}
                    deleteTask={deleteTask}
                    openDelete={openDelete}
                    closeDelete={closeDelete}
                />
                    
            </div>
        </Dialog>
    )
}

export default EditTask;