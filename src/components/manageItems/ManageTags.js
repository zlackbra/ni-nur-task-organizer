import {Button} from "@mui/material";

const ManageTags = ({setManageTagsActive}) => {
    return <Button variant="contained" style={{background: 'rgb(191,175,238)', borderRadius: '20px', color: 'black'}} 
        onClick={(e) => {
            setManageTagsActive(true); 
            e.currentTarget.blur()}}>
        Tags
    </Button>
}

export default ManageTags;