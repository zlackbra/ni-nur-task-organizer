import {Button} from "@mui/material";

const ManageStates = ({setManageStatesActive}) => {
    return <Button variant="contained" style={{background: 'rgb(191,175,238)', borderRadius: '20px', color: 'black'}} 
    onClick={(e) => {
        setManageStatesActive(true);
        e.currentTarget.blur()}}>
        States
    </Button>
}

export default ManageStates;