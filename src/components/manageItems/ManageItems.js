import React, {useState} from "react";

import "./style/ManageItems.css"
import Button from "@mui/material/Button";
import {ManageItemsContext, ManageItemsSetContext} from "./ManageItemsContext";
import {ManageItemsFields} from "./ManageItemsFields";
import Dialog from "@mui/material/Dialog";
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Cancel';

function Header({name}) {
    return (
        <div className="header">
            <h1>{name}</h1>
        </div>
    );
}


function Footer({save, close}) {
    return (
        <div className="footer">
            <div className="buttons">
                <Button variant="contained" startIcon={<SaveIcon/>} onClick={save}>
                    Save
                </Button>
                <Button variant="outlined" startIcon={<CancelIcon/>} onClick={close}>
                    Close
                </Button>
            </div>
        </div>
    );
}


export function ManageItems({initItems, setItems, name, item, close, color}) {
    const [items, set] = useState(initItems);

    const saveAndClose = () => {
        setItems(items)
        close()
    }

    const handleKeyDown = (event) => {
        if (event.key === 'Enter'){
            saveAndClose();
        }
        else if (event.key === 'Escape'){
            close();
        }
      };

    return (
        <Dialog
            open={true}
            fullWidth
            maxWidth="md"
            onKeyDown={handleKeyDown}
        >
            <div className="manage-tags-dialog">
                <Header name={name}/>
                <ManageItemsContext.Provider value={items}>
                    <ManageItemsSetContext.Provider value={set}>
                        <ManageItemsFields color={color} item={item}/>
                    </ManageItemsSetContext.Provider>
                </ManageItemsContext.Provider>

                <Footer close={close} save={saveAndClose}/>
            </div>
        </Dialog>
    );
}
