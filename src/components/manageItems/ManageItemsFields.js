import {useContext, useEffect, useState} from "react";
import {ManageItemsContext, ManageItemsSetContext} from "./ManageItemsContext";
import {Box, Grid, IconButton, List, ListItem, ListItemText, Stack} from "@mui/material";
import ClearIcon from '@mui/icons-material/Clear';
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import TaskCard from "../taskCard/TaskCard";
import AddIcon from '@mui/icons-material/Add';

function ManagedItem({item, deleteItem}) {
    return (
        <ListItem
            component="div"
            secondaryAction={
                <IconButton edge="end" aria-label="delete" onClick={() => deleteItem(item)}>
                    <ClearIcon/>
                </IconButton>
            }>
            <ListItemText
                sx={{borderBottom: 'black solid 1px'}}
                overflow="hidden"
                primary={item}
                secondary={null}
            />
        </ListItem>
    );
}


export function ManageItemsFields({color, item}) {
    const items = useContext(ManageItemsContext);
    const setItems = useContext(ManageItemsSetContext);

    const [newItem, setNewItem] = useState("");

    const textFieldLabel = "New " + item
    const addItemLabel = "Add " + item

    return (
        <Stack alignItems="center" justifyContent="center" >
            <Stack direction="row" spacing={3} alignItems="center" margin={2} >
                <TextField
                    style={{background: color, borderRadius: '5px', width: "200px"}}
                    size="small"
                    error={ newItem.trim().length === 0 && newItem !== ""}
                    label={textFieldLabel}
                    variant="outlined"
                    onChange={ (event) => setNewItem(event.target.value) }
                ></TextField>
                <div className="buttons">
                    <Button
                        variant="outlined"
                        startIcon={<AddIcon/>}
                        onClick={ () => {
                            if (newItem.trim().length !== 0) {
                                const filtered = items.filter(other => other !== newItem)
                                setItems([...filtered, newItem])
                            }
                        }}
                    >{addItemLabel}
                    </Button>
                </div>
            </Stack>

            <Box sx={{ width: '100%', height: 400, maxWidth: 348, maxHeight: 200, overflowY: "auto", margin: 2, border: 1, borderColor: "grey.500", borderRadius: "5px", background: color,}} >
                <List dense>
                    {items
                        .sort((a, b) => a.localeCompare(b))
                        .map(item => <ManagedItem item={item} deleteItem={() => setItems(items.filter(other => other !== item))} /> )
                    }
                </List>
            </Box>
        </Stack>
    );
}