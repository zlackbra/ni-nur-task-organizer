import React, {useContext, useEffect, useState} from "react";
import {Autocomplete, Stack} from "@mui/material";
import TextField from '@mui/material/TextField';
import {DatePicker, DateTimePicker, LocalizationProvider} from "@mui/x-date-pickers";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";
import {FilterTaskContext, FilterTaskDispatchContext} from "./FilterTaskContext";
import "./style/FilterTaskFields.css"
import { StateBgColor, TagBgColor } from "../../consts/WindowConsts";

function ItemsField({allItems, initItems, setItems, name, bgColor='rgb(224, 224, 224)'}) {
    return (
        <>
            <h2>{name}</h2>
            <Autocomplete
                multiple
                options={allItems}
                value={initItems}
                onChange={(event, options) => {
                    setItems(options);
                }}
                renderInput={(params) => (
                    <div style={{background: bgColor, borderRadius: '5px', }}>
                        <TextField {...params} placeholder="Search"/>
                    </div>
                )}
            />
        </>
    );
}


function DatesField({initDates, name, setDateFrom, setDateTo}) {
    const dates = initDates ? initDates : {from: null, to: null}

    const validateDate = (newValue, set) => {
        if (newValue == null || newValue.toString() != 'Invalid Date') {
            set(newValue)
        }
    }
    
    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
            <h2>{name}</h2>
            <Stack direction="row" spacing={3}>
                <DateTimePicker
                    key={name+'from'}
                    label="From"
                    value={dates.from ? dayjs(dates.from) : null}
                    onChange={(e) => validateDate(e, setDateFrom)}
                    format='YYYY-MM-DD HH:mm'
                    ampm={false}
                    slotProps={{
                        actionBar: {
                          actions: [ 'clear', 'today', 'accept', 'cancel']
                        }
                      }}
                />
                <DateTimePicker
                    key={name+'to'}
                    label="To"
                    value={dates.to ? dayjs(dates.to) : null}
                    onChange={(e) => validateDate(e, setDateTo)}
                    format='YYYY-MM-DD HH:mm'
                    ampm={false}
                    slotProps={{
                        actionBar: {
                          actions: [ 'clear', 'today', 'accept', 'cancel']
                        }
                      }}
                />
            </Stack>
        </LocalizationProvider>
    );
}


export function FilterTaskFields({tasks, tags, states, users}) {
    const filter = useContext(FilterTaskContext);
    const dispatch = useContext(FilterTaskDispatchContext);

    return (
        <>
            <ItemsField
                name="Tags"
                allItems={tags}
                bgColor={TagBgColor}
                initItems={filter.tags}
                setItems={(items) => dispatch({type: "setTags", item: items})}
            />
            <ItemsField
                name="States"
                allItems={states}
                bgColor={StateBgColor}
                initItems={filter.states}
                setItems={(items) => dispatch({type: "setStates", item: items})}
            />
            <ItemsField
                name="Names"
                allItems={tasks.map((task) => task.name)}
                initItems={filter.name}
                setItems={(items) => dispatch({type: "setNames", item: items})}
            />
            <ItemsField
                name="Assigned To"
                allItems={users.map((user) => user.name)}
                initItems={filter.assignedTo}
                setItems={(items) => dispatch({type: "setAssignedTo", item: items})}
            />
            <DatesField
                name="Created At"
                initDates={filter.createdAt}
                setDateFrom={(datetime) => dispatch({type: "setCreatedAtFrom", item: datetime})}
                setDateTo={(datetime) => dispatch({type: "setCreatedAtTo", item: datetime})}
            />
            <DatesField
                name="Deadline At"
                initDates={filter.deadline}
                setDateFrom={(datetime) => dispatch({type: "setDeadlineAtFrom", item: datetime})}
                setDateTo={(datetime) => dispatch({type: "setDeadlineAtTo", item: datetime})}
            />
        </>
    );
}