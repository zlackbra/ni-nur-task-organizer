export function FilterReducer(filter, action) {
    switch (action.type) {
        case 'setTags': {
            return {...filter, tags: action.item };
        }

        case 'setStates': {
            return {...filter, states: action.item };
        }

        case 'setAssignedTo': {
            return {...filter, assignedTo: action.item };
        }

        case 'setCreatedAtFrom': {
            return {...filter, createdAt: { ...filter.createdAt, from: action.item ? action.item : null }};
        }

        case 'setCreatedAtTo': {
            return {...filter, createdAt: { ...filter.createdAt, to: action.item ? action.item : null }};
        }

        case 'setDeadlineAtFrom': {
            return {...filter, deadline: {...filter.deadline, from: action.item ? action.item : null }};
        }

        case 'setDeadlineAtTo': {
            return {...filter, deadline: {...filter.deadline, to: action.item ? action.item : null }};
        }

        case 'setNames': {
            return {...filter, name: action.item };
        }

        default: {
            throw Error('Unknown action: ' + action.type);
        }
    }
}
