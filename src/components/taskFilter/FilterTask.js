import {useReducer} from 'react';
import Button from "@mui/material/Button";
import {FilterReducer} from "./FilterReducer";
import {FilterTaskFields} from "./FilterTaskFields";
import {FilterTaskContext, FilterTaskDispatchContext} from "./FilterTaskContext";
import "./style/FilterTask.css"
import Dialog from "@mui/material/Dialog";
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Cancel';

function Header({window}) {
    return (
        <div className="header">
            <h1>{"Task Filter for " + window}</h1>
        </div>
    );
}


function Footer({save, close}) {
    return (
        <div className="footer">
            <div className="buttons">
                <Button variant="contained" startIcon={<SaveIcon/>}  onClick={save}>
                    Save
                </Button>
                <Button variant="outlined" startIcon={<CancelIcon/>} onClick={close}>
                    Close
                </Button>
            </div>
        </div>
    );
}


export function FilterTask({tasks, tags, states, window, updateWindow, close, users}) {
    const [filter, dispatch] = useReducer(FilterReducer, window.filter ? window.filter : {});

    const updateFilterInWindow = () => {
        window.filter = filter

        updateWindow(window)
    }

    const saveAndClose = () => {
        updateFilterInWindow(filter)
        close()
    }

    const handleKeyDown = (event) => {
        if (event.key === 'Enter'){
            saveAndClose();
        }
        else if (event.key === 'Escape'){
            close();
        }
      };

    return (
        <Dialog
            open={true}
            fullWidth
            maxWidth="xl"
            onKeyDown={handleKeyDown}
        >
            <div className="filter-dialog">
                <Header window={window.name}/>

                <FilterTaskContext.Provider value={filter}>
                    <FilterTaskDispatchContext.Provider value={dispatch}>
                        <FilterTaskFields tasks={tasks} tags={tags} states={states} users={users}/>
                    </FilterTaskDispatchContext.Provider>
                </FilterTaskContext.Provider>

                <Footer close={close} save={saveAndClose}/>
            </div>
        </Dialog>
    );
}
