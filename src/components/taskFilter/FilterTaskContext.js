import {createContext} from 'react';


export const FilterTaskContext = createContext(null);
export const FilterTaskDispatchContext = createContext(null);