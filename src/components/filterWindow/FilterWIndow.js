import {useState} from 'react';
import './style/FilterWindow.css'
import {Button, Tab, Tabs} from '@mui/material';
import {StyleSheet} from 'react-native-web';
import {MaxWindows} from '../../consts/WindowConsts'
import CloseIcon from '@mui/icons-material/Close';
import FilterDialog from './FilterDialog';
import ModeEditIcon from '@mui/icons-material/ModeEdit';

const FilterWindow = ({windows, addWindow, removeWindow, updateWindow, setActiveWindow, activeWindow}) => {
    const [activeDialog, setActiveDialog] = useState(false);
    
    const openDialog = (e) => {
        e.currentTarget.blur()
        setActiveDialog(true);
        
    };

    const closeDialog = () => {
        setActiveDialog(false);
    };

    const changeName = (window, name) => {
        window.name = name;

        updateWindow(window)

        setActiveWindow(window)
        setActiveDialog(false);
    }

    if (!windows.includes(activeWindow)) {
        setActiveWindow(windows[0]);
    }

    const handleChange = (event, newValue) => {
        setActiveWindow(newValue)
    }

    return (
        <div className="tab-row" style={styles().tabRow}>
            <Tabs
                value={activeWindow}
                onChange={handleChange}
                orientation='horizontal'
                variant="scrollable"
                scrollButtons
                allowScrollButtonsMobile
                TabIndicatorProps={{
                    style: {
                        backgroundColor: 'rgb(140,116,200)' // background color
                    }
                }}
                sx={{
                    "& button": {borderRadius: 2},
                    "& button:hover": {backgroundColor: 'rgb(160,136,220)'},
                    "& button.Mui-selected": {backgroundColor: activeWindow.color},
                    maxWidth: '70vw'
                }}
            >
                {windows.map((tab, index) => (
                    <Tab
                        key={'tab' + tab.id}
                        label={tab.name}
                        value={tab}
                        onDoubleClick={(e) => {
                            openDialog(e)
                        }}
                        onClick={(e) => {
                            e.currentTarget.blur()
                        }}
                        iconPosition='end'
                        icon={
                            <>
                            <ModeEditIcon
                                id={tab.id}
                                key={tab.id + "-editIcon"} /* error in console because of same keys */
                                disabled={windows.length <= 1}
                                onClick={(e) => {
                                    openDialog(e)
                                }}
                            />
                            <CloseIcon
                                id={tab.id}
                                key={tab.id + "-closeIcon"} /* error in console because of same keys */
                                disabled={windows.length <= 1}
                                onClick={(e) => {
                                    removeWindow(tab)
                                }}
                            />
                            </>
                        }/>
                ))}
            </Tabs>
            <Button
                variant="contained"
                style={{
                    maxWidth: '30px',
                    maxHeight: '30px',
                    minWidth: '30px',
                    minHeight: '30px',
                    fontSize: '2rem',
                    backgroundColor: 'rgb(110,86,170)'
                }}
                size='large'
                onClick={addWindow}
                disabled={windows.length >= MaxWindows}
            >+</Button>
            {activeDialog && <FilterDialog
                open={true} tab={activeWindow} close={closeDialog} save={changeName}/>
            }
        </div>
    )
}

export default FilterWindow;

const styles = () => StyleSheet.create({
        tabRow: {
            height: '80px',
            top: '40px',
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
        }
    }
);