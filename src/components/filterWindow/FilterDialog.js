import { useState } from "react";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import { Button } from "@mui/material";
import CancelIcon from '@mui/icons-material/Cancel';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

const FilterDialog = ({open, tab, close, save}) => {

    const validateName = (name) => {
        return (name.length > 0 && name.length < 20 && name.trim().length !== 0)
    }

    const [name, setName] = useState(tab.name);
    const [valid, setValid] = useState(validateName(tab.name));

    const onChange = (e) => {
        const n = e.target.value
        const v = validateName(n)
        setValid(v)
        console.log(v)
        console.log(n)
        if (v) {
            setName(n)
        }
    }

    const handleKeyDown = (event) => {
        if (event.key === 'Enter'){
            save(tab, name);
        }
        else if (event.key === 'Escape'){
            close();
        }
      };

    return (
        <Dialog open={open} onClose={close}     
        onKeyDown={handleKeyDown}
        sx={{
            "& .MuiDialog-container": {
              "& .MuiPaper-root": {
                width: "100%",
                maxWidth: "500px",  // Set your width here
              },
            },
          }}>
            <DialogTitle>Change tab name</DialogTitle>
                <DialogContent>
                <TextField
                    autoFocus
                    margin="dense"
                    id="name"
                    label="Tab name"
                    type="text"
                    defaultValue={name}
                    fullWidth
                    variant="standard"
                    onChange={onChange}
                    error={!valid}
                    helperText={!valid ? 'Name must be filled and less than 20 characters long!' : ' '}
                />
                </DialogContent>
                <DialogActions>
                    <Button variant='contained' onClick={(e) => {save(tab, name)}} startIcon={<CheckCircleIcon/>}>Accept</Button>
                    <Button onClick={close} variant="outlined" startIcon={<CancelIcon/>}>Close</Button>
            </DialogActions>
        </Dialog>
);
}


export default FilterDialog;