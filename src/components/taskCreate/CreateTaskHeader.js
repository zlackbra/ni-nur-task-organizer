import './style/CreateTaskHeader.css'

const CreateTaskHeader = () => {
    return (
        <div className="create-task-header">
            <h1>Create Task</h1>
        </div>
    )
}

export default CreateTaskHeader;