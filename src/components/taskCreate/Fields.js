import './style/Fields.css'
import Description from "./fieldrows/Description";
import Name from "./fieldrows/Name";
import Assign from "./fieldrows/Assign";
import CreatedAt from "./fieldrows/CreatedAt";
import Deadline from "./fieldrows/Deadline";
import State from "./fieldrows/State";
import Tags from "./fieldrows/Tags";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import {LocalizationProvider} from "@mui/x-date-pickers";
import {useEffect, useState} from "react";

const Fields = ({setName, setAssign, setDeadline, setTags, setState, setDescription, createdAt}) => {
    const [fieldsTable, setFieldsTable] = useState(
        window.innerWidth > 850 ? "fields-table" : "fields-column"
    )
    useEffect(() => {
        function handleResize() {
            setFieldsTable(window.innerWidth > 850 ? "fields-table" : "fields-column")
        }

        window.addEventListener("resize", handleResize)

        return _ => {
            window.removeEventListener('resize', handleResize)
        }
    })

    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
            <div className={fieldsTable}>
                <div className="name-assign">
                    <Name setName={setName}/>
                    <Assign setAssign={setAssign}/>
                </div>
                <div className="dates-labels">
                    <div className="dates">
                        <div>
                            <h2>Dates</h2>
                        </div>
                        <CreatedAt createdAt={createdAt}/>
                        <Deadline setDeadline={setDeadline}/>
                    </div>
                    <div className="tags-state">
                        <Tags setTags={setTags}/>
                        <State setState={setState}/>
                    </div>
                </div>
            </div>
            <Description setDescription={setDescription}/>
        </LocalizationProvider>
    )
}

export default Fields;