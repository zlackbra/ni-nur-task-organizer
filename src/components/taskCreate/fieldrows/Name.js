import './style/Name.css'
import TextField from "@mui/material/TextField";
import {useState} from "react";

const Name = ({setName}) => {
    const [nameForValidation, setNameForValidation] = useState("")

    const setNameForValidationAsWell = (name) => {
        setName(name)
        setNameForValidation(name)
    }

    return (
        <div className="name">
            <div className="name-title">
                <h2>Name</h2>
                <h2 className="required">*</h2>
            </div>
            <TextField
                style={{background: "white", borderRadius: '5px', width: "225px"}}
                size="small"
                error={ nameForValidation.trim().length === 0 }
                variant="outlined"
                onChange={ (event) => setNameForValidationAsWell(event.target.value) }
            ></TextField>
        </div>
    )
}

export default Name