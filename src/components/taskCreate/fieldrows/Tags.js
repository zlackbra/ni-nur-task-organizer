import './style/Tags.css'
import React, {useState} from "react";
import CreatableSelect from "react-select/creatable";
import { TagCustomStyles } from '../../../consts/WindowConsts';

const createOption = (label) => ({
    label,
    value: label.toLowerCase().replace(/\W/g, ''),
});

const Tags = ({setTags}) => {
    const initialTagsOptions = JSON.parse(localStorage.getItem("data")).tags.map(tag =>
        ({value: tag, label: tag})
    )

    const [options, setOptions] = useState(initialTagsOptions);
    const [value, setValue] = useState([]);

    const handleCreate = (inputValue) => {
        const newOption = createOption(inputValue);
        setOptions((prev) => [...prev, newOption]);

        const data = JSON.parse(localStorage.getItem("data"))
        data.tags.push(inputValue)

        localStorage.setItem(
            "data",
            JSON.stringify(data)
        )

        value.push(newOption)
        onTagsChange(value)
    };

    const onTagsChange = (newValue) => {
        setValue(newValue)

        newValue && newValue.length !== 0
            ? setTags(newValue.map(tagOption => tagOption.value))
            : setTags([])
    }

    return (
        <div className="tags">
            <h2>Tags</h2>
            <CreatableSelect
                isClearable
                options={options}
                onChange={(newValue) => onTagsChange(newValue)}
                styles={TagCustomStyles}
                isMulti
                closeMenuOnSelect={false}
                onCreateOption={handleCreate}
                value={value}
            />
        </div>
    )
}

export default Tags;