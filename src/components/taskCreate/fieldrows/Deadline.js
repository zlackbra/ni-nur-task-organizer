import {DateTimePicker} from "@mui/x-date-pickers";
import './style/Deadline.css'
import dayjs from "dayjs";

const Deadline = ({setDeadline}) => {
    const validateDate = (newValue) => {
        if (newValue == null || newValue.toString() != 'Invalid Date') {
            setDeadline(newValue)
        }
    }

    return (
        <div className="deadline-cr">
            <h3>Deadline</h3>
            <DateTimePicker
                onChange={validateDate}
                ampm={false}
                defaultValue={dayjs().add(1, 'week')}
                format='YYYY-MM-DD HH:mm'
                slotProps={{
                    actionBar: {
                      actions: ['today', 'accept', 'cancel']
                    }
                  }}
            />
        </div>
    )
}

export default Deadline;