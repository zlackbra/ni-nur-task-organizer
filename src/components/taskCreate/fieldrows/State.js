import './style/State.css'
import CreatableSelect from 'react-select/creatable';
import React, {useState} from "react";

const customStyles = {
    control: (base) => ({
        ...base,
        background: 'rgba(255, 255, 200)',
        width: '230px'
    })
};

const createOption = (label) => ({
    label,
    value: label.toLowerCase().replace(/\W/g, ''),
});

const State = ({setState}) => {
    const initialStatesOptions = JSON.parse(localStorage.getItem("data")).states.map(state =>
        ({value: state, label: state})
    )

    const [options, setOptions] = useState(initialStatesOptions);
    const [value, setValue] = useState([]);

    const handleCreate = (inputValue) => {
        const newOption = createOption(inputValue);
        setOptions((prev) => [...prev, newOption]);

        const data = JSON.parse(localStorage.getItem("data"))
        data.states.push(inputValue)

        localStorage.setItem(
            "data",
            JSON.stringify(data)
        )
        onStateChange(newOption)
    };

    const onStateChange = (newValue) => {
        setValue(newValue)

        newValue ? setState(newValue.value) : setState(null)
    }

    return (
        <div className="state">
            <h2>State</h2>
            <CreatableSelect
                isClearable
                options={options}
                onChange={(newValue) => onStateChange(newValue)}
                styles={customStyles}
                onCreateOption={handleCreate}
                value={value}
            />
        </div>
    )
}

export default State;