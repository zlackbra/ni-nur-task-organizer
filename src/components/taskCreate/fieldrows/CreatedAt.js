import {DateTimePicker} from "@mui/x-date-pickers";
import './style/CreatedAt.css'
import dayjs from "dayjs";

const CreatedAt = ({createdAt}) => {
    const formatPartOfDate = (datePart, numberOfChars) => {
        const datePartString = datePart.toString()
        const difference = numberOfChars - datePartString.length

        return difference !== 0 ? "0".repeat(difference) + datePartString : datePartString
    }

    const formatDate = (date) => {
        const day = formatPartOfDate(date.getDate(), 2)
        const month = formatPartOfDate(date.getMonth() + 1, 2)
        const year = formatPartOfDate(date.getFullYear(), 4)
        const hours = formatPartOfDate(date.getHours(), 2)
        const minutes = formatPartOfDate(date.getMinutes(), 2)

        return day + "-" + month + "-" + year + " " + hours + ":" + minutes
    }

    return (
        <div className="created-at">
            <h3>Created At</h3>
            <DateTimePicker
                disabled={true}
                selected={createdAt}
                defaultValue={dayjs(createdAt)}
                format='YYYY-MM-DD HH:mm'
            />
        </div>
    )
}

export default CreatedAt;