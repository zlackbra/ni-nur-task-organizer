import './style/Description.css'

const Description = ({setDescription}) => {
    return (
        <div className="description">
            <h2>Description</h2>
            <textarea className="text" onChange={event => setDescription(event.target.value)}/>
        </div>
    )
}

export default Description;