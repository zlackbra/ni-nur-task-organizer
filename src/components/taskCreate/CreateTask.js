import "./style/CreateTask.css"
import {useState} from "react";
import Footer from "./Footer";
import CreateTaskHeader from "./CreateTaskHeader";
import Fields from "./Fields";
import {v4 as uuid} from "uuid";
import Dialog from "@mui/material/Dialog";
import dayjs from "dayjs";

const CreateTask = ({setActive, addTask}) => {
    const [isInvalid, setIsInvalid] = useState(false)
    const [name, setName] = useState(null)
    const createdAt = new Date()
    const [deadline, setDeadline] = useState(dayjs().add(1, 'week'))
    const [assign, setAssign] = useState(null)
    const [description, setDescription] = useState(null)
    const [tags, setTags] = useState([])
    const [state, setState] = useState(null)

    const save = () => {
        // validation
        if (name == null || name === "" || name.length > 50 || name.trim().length === 0) {
            setIsInvalid(true)
        } else {
            setActive(false)

            const newTask = {
                id: uuid(),
                name: name.trim(),
                createdAt: createdAt.toISOString(),
                deadline: deadline ? deadline.toISOString() : null,
                assignedTo: assign,
                description: description,
                tags: tags,
                state: state,
                comments: []
            }

            addTask(newTask)
        }
    }

    const close = () => setActive(false)

    const handleKeyDown = (event) => {
        if (event.key === 'Enter'){
            save();
        }
        else if (event.key === 'Escape'){
            close();
        }
      };

    return (
        <Dialog
            open={true}
            fullWidth
            maxWidth="md"
            onKeyDown={handleKeyDown}
        >
            <div className='create-task-dialog'>
                <CreateTaskHeader/>
                <Fields
                    setName={setName}
                    setDeadline={setDeadline}
                    setAssign={setAssign}
                    setDescription={setDescription}
                    setTags={setTags}
                    setState={setState}
                    createdAt={createdAt}
                />
                <Footer close={close} save={save} valid={isInvalid ? "create-task-invalid" : "create-task-valid"}/>
            </div>
        </Dialog>
    )
}

export default CreateTask;