import './style/Footer.css'
import Button from '@mui/material/Button';
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Cancel';

const Footer = (props) => {
    return (
        <div className="footer">
            <div className="buttons">
                <div className={props.valid}>
                    Name field must be filled and less than 50 characters long!
                </div>
                <Button variant="contained" startIcon={<SaveIcon/>} onClick={props.save} style={{ minWidth: '100px'}}>
                    Save
                </Button>
                <Button variant="outlined" startIcon={<CancelIcon/>} onClick={props.close} style={{ minWidth: '100px'}}>
                    Close
                </Button>
            </div>
        </div>
    )
}

export default Footer;