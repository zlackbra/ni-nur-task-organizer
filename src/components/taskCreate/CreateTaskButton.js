import {Button} from "@mui/material";

const CreateTaskButton = ({setCreateTaskActive}) => {
    return <Button variant="contained" style={{background: 'rgb(191,175,238)', borderRadius: '20px', color: 'black'}} 
        onClick={(e) => {
            setCreateTaskActive(true)
            e.currentTarget.blur()
        }
    }
    >
        Create Task
    </Button>
}

export default CreateTaskButton;