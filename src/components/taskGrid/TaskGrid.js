import {Grid} from "@mui/material";
import TaskCard from "../taskCard/TaskCard";
import {filterTasks, sortTasks} from "../../data/DataOps";


const TaskGrid = ({tasks, updateTask, removeTask, filter, sort, updateComments, addTag, addState}) => {
    const filteredSortedTasks = sortTasks(filterTasks(tasks, filter), sort)

    return (
        <div className='task-grid-div'>
            <Grid container
                  alignItems='center'
                  justifyContent='center'
                  padding='5%'
                  paddingTop='0%'>
                {filteredSortedTasks.map((task) => (
                    <Grid item key={task.id} m={'2%'}>
                        <TaskCard
                            key={task.id}
                            task={task}
                            updateTask={updateTask}
                            removeTask={removeTask}
                            updateComments={updateComments}
                            addTag={addTag}
                            addState={addState}
                        />
                    </Grid>
                ))}
            </Grid>
        </div>
    );
}

export default TaskGrid;