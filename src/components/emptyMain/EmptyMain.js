import './style/EmptyMain.css'
import { Button } from '@mui/material';

const EmptyMain = ({addTab}) => {

    return (
        <div className="empty-main">
            <div className='button-area'>
                <Button
                    style={{fontSize: '2rem'}}
                    variant='contained'
                    onClick={addTab}
                >+</Button>
                <h2>
                    Create your first tab
                </h2>
            </div>
        </div>
    );
}

export default EmptyMain;