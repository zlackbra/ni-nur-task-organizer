import createTab from "../common/createTab";
import EmptyMain from "../emptyMain/EmptyMain";
import FilterWindow from "../filterWindow/FilterWIndow";
import TaskGrid from "../taskGrid/TaskGrid";
import {useState} from 'react';
import {MaxWindows, StateBgColor, TagBgColor} from "../../consts/WindowConsts";
import "./style/MainContent.css"
import NavBar from "../../navbar/NavBar";
import CreateTaskButton from "../taskCreate/CreateTaskButton";
import ManageTags from "../manageItems/ManageTags";
import ManageStates from "../manageItems/ManageStates";
import CreateTask from "../taskCreate/CreateTask";
import Button from "@mui/material/Button";
import {ManageItems} from "../manageItems/ManageItems";
import {removeDeletedStatesEverywhere, removeDeletedTagsEverywhere} from "../../data/DataOps";
import {FilterTask} from "../taskFilter/FilterTask";
import {SortTask} from "../taskSort/SortTask";
import Undo from "../common/Undo";
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import SortIcon from '@mui/icons-material/Sort';

const MainContent = ({dataFromStorage}) => {
    // initialize each object of the data
    const [windows, setWindows] = useState(dataFromStorage.windows ? dataFromStorage.windows : []);
    const [tasks, setTasks] = useState(dataFromStorage.tasks ? dataFromStorage.tasks : [])
    const [tags, setTags] = useState(dataFromStorage.tags ? dataFromStorage.tags : [])
    const [states, setStates] = useState(dataFromStorage.states ? dataFromStorage.states : [])
    const [comments, setComments] = useState(dataFromStorage.comments ? dataFromStorage.comments : [])
    const [colors, setColors] = useState(dataFromStorage.colors ? dataFromStorage.colors : [])
    const users = dataFromStorage.users // state not necessary, won't change
    // ======== color of window push, pop =========
    const pushColor = (color) => {
        colors.push(color)

        setColors(colors)

        const data = JSON.parse(localStorage.getItem("data"));
        data.colors.push(color);
        localStorage.setItem(
            "data",
            JSON.stringify(data)
        )
    }
    const popColor = () => {
        const color = colors.pop()

        setColors(colors)

        const data = JSON.parse(localStorage.getItem("data"));
        data.colors = colors.filter((item) => item !== window)
        localStorage.setItem(
            "data",
            JSON.stringify(data)
        )

        return color
    }
    // ======== window create, delete, update =========
    const addWindow = () => {
        // check for max windows
        if (windows.length >= MaxWindows) return;
        // add to state
        const newWindow = createTab(popColor())
        const updatedWindows = [...windows, newWindow]
        setWindows(updatedWindows);
        // add to storage
        const data = JSON.parse(localStorage.getItem("data"));
        data.windows.push(newWindow);
        localStorage.setItem(
            "data",
            JSON.stringify(data)
        )
        // additional modifications
        setEmpty(false);
        setActiveWindow(newWindow)
    }
    const removeWindow = (window) => {
        // remove from state
        const newWindows = windows.filter((item) => item.id !== window.id);
        if (newWindows.length === 0) {
            setEmpty(true)
            setActiveWindow(null)
        }
        setWindows(newWindows)
        // remove from storage
        const data = JSON.parse(localStorage.getItem("data"));
        data.windows = newWindows;
        localStorage.setItem(
            "data",
            JSON.stringify(data)
        )
        // additional modifications
        pushColor(window.color)
    }
    const updateWindow = (window) => {
        // update in state
        const newWindowsWithUpdated = windows.map((windowInState) => {
                if (windowInState.id === window.id) {
                    return window
                } else {
                    return windowInState
                }
            }
        )
        setWindows(newWindowsWithUpdated)
        // update in storage
        const data = JSON.parse(localStorage.getItem("data"));
        data.windows = newWindowsWithUpdated;
        localStorage.setItem(
            "data",
            JSON.stringify(data)
        )
    }
    // ======== task create, delete, update =========
    const addTask = (task) => {
        // add to state
        const updatedTasks = [...tasks, task]
        setTasks(updatedTasks);
        // add to storage
        const data = JSON.parse(localStorage.getItem("data"));
        data.tasks.push(task);
        localStorage.setItem(
            "data",
            JSON.stringify(data)
        )
    }
    const removeTask = (task) => {
        // remove from state
        const newTasks = tasks.filter((item) => item.id !== task.id);
        setTasks(newTasks)
        setUndoName(task.name)
        // remove from storage
        const data = JSON.parse(localStorage.getItem("data"));
        data.tasks = newTasks;
        data.lastDeleted = task;
        localStorage.setItem(
            "data",
            JSON.stringify(data)
        )
    }

    const undoRemoval = () => {
        const data = JSON.parse(localStorage.getItem("data"));
        const deletedTask = data.lastDeleted 
        addTask(deletedTask)
        setUndoName(null)
    }

    const clearUndo = () => {
        setUndoName(null)
    }

    const updateTask = (task) => {
        // update in state
        const newTasksWithUpdated = tasks.map((taskInState) => {
                if (taskInState.id === task.id) {
                    return task
                } else {
                    return taskInState
                }
            }
        )
        console.log(newTasksWithUpdated);
        setTasks(newTasksWithUpdated)
        // update in storage
        const data = JSON.parse(localStorage.getItem("data"));
        data.tasks = newTasksWithUpdated;
        localStorage.setItem(
            "data",
            JSON.stringify(data)
        )
    }
    // ======== tag create/delete =========
    const updateAllTags = (newTags) => {
        const updatedInfluencedData = removeDeletedTagsEverywhere(windows, tasks, newTags)
        // update in state
        setTags(newTags)
        setWindows(updatedInfluencedData.windows)
        setTasks(updatedInfluencedData.tasks)
        // remove from storage
        const data = JSON.parse(localStorage.getItem("data"));
        data.tags = newTags;
        data.windows = updatedInfluencedData.windows
        data.tasks = updatedInfluencedData.tasks
        localStorage.setItem(
            "data",
            JSON.stringify(data)
        )
    }
    const addTag = (newTag) => {
        // add to state
        const updatedTags = [...tags, newTag]
        setTags(updatedTags);
        // add to storage
        const data = JSON.parse(localStorage.getItem("data"));
        data.tags.push(newTag);
        localStorage.setItem(
            "data",
            JSON.stringify(data)
        )
    }
    // ======== state create/delete =========
    const updateAllStates = (newStates) => {
        const updatedInfluencedData = removeDeletedStatesEverywhere(windows, tasks, newStates)
        // update in state
        setStates(newStates)
        setWindows(updatedInfluencedData.windows)
        setTasks(updatedInfluencedData.tasks)
        // remove from storage
        const data = JSON.parse(localStorage.getItem("data"));
        data.states = newStates;
        data.windows = updatedInfluencedData.windows
        data.tasks = updatedInfluencedData.tasks
        localStorage.setItem(
            "data",
            JSON.stringify(data)
        )
    }
    const addState = (newState) => {
        // add to state
        const updatedStates = [...states, newState]
        setStates(updatedStates);
        // add to storage
        const data = JSON.parse(localStorage.getItem("data"));
        data.states.push(newState);
        localStorage.setItem(
            "data",
            JSON.stringify(data)
        )
    }
    // ======== comments update =========
    const updateComments = (newComment) => {
        // add to state
        const updatedComments = [...comments, newComment]
        setComments(updatedComments);
        // add to storage
        // const data = JSON.parse(localStorage.getItem("data"));
        // data.comments.push(newComment);
        // localStorage.setItem(
        //     "data",
        //     JSON.stringify(data)
        // )
    }

    const [empty, setEmpty] = useState(windows.length === 0);
    const [activeWindow, setActiveWindow] = useState(empty ? null : windows[0])
    const [createTaskActive, setCreateTaskActive] = useState(false)
    const [manageTagsActive, setManageTagsActive] = useState(false)
    const [manageStatesActive, setManageStatesActive] = useState(false)
    const [filterActive, setFilterActive] = useState(false)
    const [sortActive, setSortActive] = useState(false)
    const [undoName, setUndoName] = useState(null)

    return (
        <>
            {!empty && <>
                <div className="sticky-header">
                    <NavBar/>
                    <div className="windows-buttons">
                        <FilterWindow windows={windows} addWindow={addWindow} removeWindow={removeWindow}
                                      updateWindow={updateWindow} setActiveWindow={setActiveWindow} activeWindow={activeWindow}/>
                        <div className="main-buttons">
                            <CreateTaskButton setCreateTaskActive={setCreateTaskActive}/>
                            <ManageTags setManageTagsActive={setManageTagsActive}/>
                            <ManageStates setManageStatesActive={setManageStatesActive}/>
                        </div>
                    </div>
                    <div className="filter-search">
                        <Button variant="contained" style={{
                            background: activeWindow.color,
                            color: 'black',
                            marginRight: '40px',
                            maxWidth: '80px',
                            minWidth: '80px'
                        }}
                                startIcon={<FilterAltIcon/>}
                                onClick={(e) => {
                                    e.currentTarget.blur() 
                                    setFilterActive(true)
                                }}
                        >
                            Filter
                        </Button>
                        <Button variant="contained"
                                startIcon={<SortIcon/>}
                                style={{background: activeWindow.color, color: 'black', maxWidth: '80px', minWidth: '80px'}}
                                onClick={(e) => {
                                    e.currentTarget.blur() 
                                    setSortActive(true)
                                }}
                        >
                            Sort
                        </Button>
                    </div>
                </div>
                <TaskGrid
                    tasks={tasks}
                    updateTask={updateTask}
                    removeTask={removeTask}
                    filter={activeWindow.filter}
                    sort={activeWindow.sort}
                    updateComments={updateComments}
                    addTag={addTag}
                    addState={addState}
                />
                {createTaskActive && <>
                    <CreateTask setActive={setCreateTaskActive} addTask={addTask}/>
                </>}
                {manageTagsActive && <>
                    <ManageItems color={TagBgColor} initItems={tags} name="Tags" item="tag" setItems={updateAllTags} close={() => setManageTagsActive(false)}/>
                </>}
                {manageStatesActive && <>
                    <ManageItems color={StateBgColor} initItems={states} name="States" item="state" setItems={updateAllStates} close={() => setManageStatesActive(false)}/>
                </>}
                {sortActive && <>
                    <SortTask window={activeWindow} close={() => setSortActive(false)} updateWindow={updateWindow}/>
                </>}
                {filterActive && <>
                    <FilterTask
                        window={activeWindow}
                        tasks={tasks}
                        tags={tags}
                        states={states}
                        users={users}
                        close={() => setFilterActive(false)}
                        updateWindow={updateWindow}
                    />
                </>}
                {undoName != null && <>
                    <Undo
                        name={undoName}
                        undoDelete={undoRemoval}
                        clearUndo={clearUndo}
                    /> 
                </>}
            </>
            }
            {empty && <>
                <div className="sticky-header">
                    <NavBar/>
                    <div className="main-empty-buttons">
                        <CreateTaskButton setCreateTaskActive={setCreateTaskActive}/>
                        <ManageTags setManageTagsActive={setManageTagsActive}/>
                        <ManageStates setManageStatesActive={setManageStatesActive}/>
                    </div>
                </div>
                <EmptyMain addTab={addWindow}/>
                {createTaskActive && <>
                    <CreateTask setActive={setCreateTaskActive} addTask={addTask}/>
                </>}
                {manageTagsActive && <>
                    <ManageItems initItems={tags} name="Tags" setItems={setTags} close={() => setManageTagsActive(false)}/>
                </>}
                {manageStatesActive && <>
                    <ManageItems initItems={states} name="States" setItems={setStates} close={() => setManageStatesActive(false)}/>
                </>}
            </>
            }
        </>
    );
}

export default MainContent;