import {DatePicker} from "@mui/x-date-pickers";
import './style/Deadline.css'

const Deadline = ({date, readOnly=0}) => {
    return (
        <div className="deadline">
            <h3>Deadline</h3>
            <DatePicker defaultValue={date} readOnly={readOnly}/>
        </div>
    )
}

export default Deadline;