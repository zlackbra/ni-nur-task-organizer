import { Button, Grid } from "@mui/material";
import "./style/TaskCard.css"
import {useState} from "react";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import {LocalizationProvider} from "@mui/x-date-pickers";
import {Text, StyleSheet} from 'react-native';
import EditTask from "../taskEdit/EditTask";
import Tags from "./Tags";
import State from "./State";


const TaskCard = ({task, updateTask, removeTask, updateComments, addTag, addState}) => {
    const [editActive, setEditActive] = useState(false)

    const openEdit = (e) => {
        e.currentTarget.blur()
        setEditActive(true);
    }

    const closeEdit = () => {
        setEditActive(false);
    }

    return (
        <>
            <button className="task-card-button" onClick={(e) => {openEdit(e)}}>
                <div className="task-card-header">
                    <Text style={styles.headerText} numberOfLines={1}>
                        {task.name}
                    </Text>
                </div>
                <div className="task-card-body">
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <Grid container rowSpacing={1} direction="column">
                        <Grid item>
                            <Grid container direction="row" className="grid-form">
                                <Grid item className="grid-form-text">Assigned to:</Grid>
                                <Grid item className="grid-item">{task.assignedTo}</Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Grid container direction="row" className="grid-form">
                                <Grid item className="grid-form-text">Deadline:</Grid>
                                <Grid item className="grid-item">
                                    {task.deadline.split('T')[0]}
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Grid container direction="row" className="grid-form">
                                <Grid item className="grid-item-list-text">State: </Grid>
                                <Grid item className="grid-item-list">
                                        <State state={task.state}/>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Grid container direction="row" className="grid-form">
                                <Grid item className="grid-item-list-text">Tags:</Grid>
                                <Grid item className="grid-item-list">
                                    <Tags setTags={null} tags={task.tags}/>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    </LocalizationProvider>
                </div>
            </button>
            {editActive && <EditTask
                task={task}
                updateTask={updateTask}
                removeTask={removeTask}
                updateComments={updateComments}
                close={closeEdit}
                addTag={addTag}
                addState={addState}
            />}
        </>
    );
}

const styles = StyleSheet.create({
    headerText: {
        fontWeight: 'bold',
        textAlign: 'left',
        padding: '5%',
        marginTop: '1%',
        fontSize: 15,
        width: '80%'
    },
  });



export default TaskCard;