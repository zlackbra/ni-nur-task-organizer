import './style/State.css'
import CreatableSelect from 'react-select/creatable';
import React, {useEffect, useState} from "react";
import {StateBgColor, TaskCardFontSize} from '../../consts/WindowConsts'

const customStyles = {
    control: (base) => ({
        ...base,
        boxShadow: 'none',
        border: 0,
        background: StateBgColor,
        width: '90px',
        minHeight: '25px',
        height: '25px',
        fontSize: TaskCardFontSize,
    }),

};

const createOption = (label) => ({
    label,
    value: label.toLowerCase().replace(/\W/g, ''),
});

const State = ({setState, state}) => {
    const initialStatesOptions = JSON.parse(localStorage.getItem("data")).states.map(state =>
        ({value: state, label: state})
    )

    const alreadyAddedState = ({value: state, label: state})

    const [options, setOptions] = useState(initialStatesOptions);
    const [value, setValue] = useState(alreadyAddedState);

    const handleCreate = (inputValue) => {
        const newOption = createOption(inputValue);
        setOptions((prev) => [...prev, newOption]);

        const data = JSON.parse(localStorage.getItem("data"))
        data.states.push(inputValue)

        localStorage.setItem(
            "data",
            JSON.stringify(data)
        )
        onStateChange(newOption)
    };

    useEffect(() => {
        setOptions(initialStatesOptions);
        setValue(alreadyAddedState);
      }, [state]); 

    const onStateChange = (newValue) => {
        setValue(newValue)

        newValue ? setState(newValue.value) : setState(null)
    }

    return (
        <div className="card-state">
            <CreatableSelect
                options={options}
                onChange={null}
                styles={customStyles}
                onCreateOption={null}
                value={value}

                components={{ DropdownIndicator:() => null, IndicatorSeparator:() => null }}
                classNamePrefix="react-select"
                isDisabled={false}
                isSearchable={false}
                isClearable={false}
                menuIsOpen={false}
                placeholder="None"
            />
        </div>
    )
}

export default State;