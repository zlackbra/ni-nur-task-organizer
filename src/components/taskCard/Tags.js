import './style/Tags.css'
import React, {useEffect, useState} from "react";
import CreatableSelect from "react-select/creatable";
import {TagBgColor, TaskCardFontSize} from '../../consts/WindowConsts'


const Tags = ({setTags, tags}) => {
    const initialTagsOptions = JSON.parse(localStorage.getItem("data")).tags.map(tag =>
        ({value: tag, label: tag})
    )

    if (tags == null) {
        tags = []
    }


    const customStyles = {
        control: (base) => ({
            ...base,
            boxShadow: 'none',
            border: 0,
            background: TagBgColor,
            width: '90px',
            minHeight: '25px',
            height: '25px',
            fontSize: TaskCardFontSize,
        }),
        valueContainer: (base) => ({
            ...base,
            flexWrap: 'unset',
            overflowX: 'auto',
            justifyContent: tags.length <= 1 ? 'center' : 'flex-start'
        }),
        multiValue: (base) => ({
            ...base,
            paddingRight: '2%', 
            display: 'inline-block',
            minWidth: '50px',
        }),
    };


    const alreadyAddedTags = tags.map(tag =>
        ({value: tag, label: tag})
    )

    const [options, setOptions] = useState(initialTagsOptions);
    const [value, setValue] = useState(alreadyAddedTags);


    useEffect(() => {
        setOptions(initialTagsOptions);
        setValue(alreadyAddedTags);
      }, [tags]); 


    return (
        <div className="card-tags">
            <CreatableSelect
                options={options}
                onChange={null}
                styles={customStyles}
                isMulti
                closeMenuOnSelect={false}
                onCreateOption={null}
                value={value}

                components={{ DropdownIndicator:() => null, IndicatorSeparator:() => null }}
                classNamePrefix="react-select"
                isDisabled={false}
                isSearchable={false}
                isClearable={false}
                menuIsOpen={false}
                placeholder="None"
            />
        </div>
    )
}

export default Tags;