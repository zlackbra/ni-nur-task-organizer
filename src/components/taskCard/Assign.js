import './style/Assign.css'

const Assign = ({name}) => {
    return (
        <div className="assign">
            <div className='assign text'>
                Assign To
            </div>
            <form className="assign-form">
                <label className="assign-label">
                    <input type="text" name="assign" value={name}/>
                </label>
            </form>
        </div>
    )
}

export default Assign;