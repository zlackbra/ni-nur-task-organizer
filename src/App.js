import data from "./data/DataEmpty"
import "./App.css"
import MainContent from "./components/mainContent/MainContent";
import EditTask from "./components/taskEdit/EditTask";

function App() {
    // localStorage.clear()
    const jsonData = localStorage.getItem("data")

    if (jsonData == null) {
        localStorage.setItem("data", JSON.stringify(data))
    }

    const dataFromStorage = JSON.parse(localStorage.getItem("data"))

    return (
        <div className="app">
            <MainContent dataFromStorage={dataFromStorage}/>
        </div>
    );
}

export default App;
